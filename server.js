require("dotenv").config();
const express = require("express");
const path = require("path");
const app = express();
const morgan = require("morgan");
const api = require("./api");
const bodyParser = require("body-parser");
const port = 3073;

app.use(
  morgan("dev"),
  bodyParser.json({ extended: false }),
  bodyParser.urlencoded({ extended: false })
);
app.use(express.static("public"), express.static("dist"));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
});
app.get("/table", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/table.html"));
});

app.use("/api", api);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});